package main

import (
	"fmt"
  "strconv"
	// "unicode"
)


func getFirstInt(line string) (int, error) {
  for _, char := range line {
    if s, err := strconv.Atoi(string(char)); err == nil {
      return s, nil
    }
  }
  return 0, nil
}


func getLastInt(line string) (int, error) {
  for i := range line {
    if s, err := strconv.Atoi(string(line[len(line) - 1 - i])); err == nil {
      return s, nil
    }
  }
  return 0, nil
}


func main_one() {
  var sum int = 0
  lines, _ := readInput("./A11input.txt")

  for _, line := range lines{
    fmt.Println(line)
    firstInt, _ := getFirstInt(line)
    fmt.Println(firstInt)
    lastInt, _ := getLastInt(line)
    fmt.Println(lastInt)
    result, _ := strconv.Atoi(fmt.Sprintf("%d%d", firstInt, lastInt))
    sum += result
  }

  fmt.Println(sum)
  
}
