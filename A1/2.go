package main

import (
	"fmt"
  "regexp"
  "strconv"
)

// TODO: Not working because of lookaheads


func getCount(line string, re *regexp.Regexp) (int, error) {
  var firstNum int
  var lastNum int
  intMapping := map[string]int{
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
  }

  matches := re.FindAllString(line, -1)

  firstNumFromMap, ok := intMapping[matches[0]]
  if ok {
    firstNum = firstNumFromMap
  }
  lastNumFromMap, ok := intMapping[matches[len(matches)-1]]
  if ok {
    lastNum = lastNumFromMap
  }
  result, _ := strconv.Atoi(fmt.Sprintf("%d%d", firstNum, lastNum))
  fmt.Println(result)

  return result, nil
  
}


func mainTwo() {
  re := regexp.MustCompile(`(one|two|three|four|five|six|seven|eight|nine|[1-9])`)
  lines, _ := readInput("./A11input.txt") 
  // lines, _ := readInput("./testinput.txt") 

  var sum int = 0

  for _, line := range lines {
    fmt.Println(line)
    res, _ := getCount(line, re)   
    sum += res
  }
  fmt.Println(sum)
}
