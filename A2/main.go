package main


import (
  "fmt"
)


func main() {
  res1 := mainOne()
  fmt.Println("Part 1 answer:", res1)
  res2 := mainTwo()
  fmt.Println("Part 2 answer:", res2)
}
