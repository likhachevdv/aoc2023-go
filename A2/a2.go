package main

import (
	"strconv"
	"strings"
)


func processGameTwo(gameLine string) (int) {
  gameStages := strings.Split(strings.Split(gameLine, ":")[1], ";")

  var power int = 0
  var cubeStorage = map[string]int{
    "red": 1,
    "green": 1,
    "blue": 1,
  } 
  for _, stage := range gameStages {
    dices := strings.Split(stage, ",")
    for _, dice := range dices {
      split := strings.Split(strings.Trim(dice, " "), " ")
      number, _ := strconv.Atoi(split[0])
      color := split[1]

      if cubeStorage[color] < number{
        cubeStorage[color] = number
      }
    }
  }
  power += cubeStorage["red"] * cubeStorage["green"] * cubeStorage["blue"]
  return power
}


func mainTwo() (int){
  var sum int = 0

  lines, _ := readInput("./input.txt")
  for _, line := range lines {
    res := processGameTwo(line)
    sum += res
  }
  return sum
}
