package main

import (
	"strconv"
	"strings"
)


func processGame(gameLine string, maximumCubes map[string]int) (int, bool) {
  gameId, _ := strconv.Atoi(strings.Split(strings.Split(gameLine, ":")[0], " ")[1])
  gameStages := strings.Split(strings.Split(gameLine, ":")[1], ";")

  for _, stage := range gameStages {
    var cubeStorage = map[string]int{
      "red": 0,
      "green": 0,
      "blue": 0,
    } 
    dices := strings.Split(stage, ",")
    for _, dice := range dices {
      split := strings.Split(strings.Trim(dice, " "), " ")
      number, _ := strconv.Atoi(split[0])
      color := split[1]
      cubeStorage[color] += number
    }

    if cubeStorage["red"] > maximumCubes["red"]{
      return gameId, false
    }
    if cubeStorage["green"] > maximumCubes["green"]{
      return gameId, false
    }
    if cubeStorage["blue"] > maximumCubes["blue"]{
      return gameId, false
    }
  }

  return gameId, true
}


func mainOne() (int){
  var maximumCubes = map[string]int{
    "red": 12,
    "green": 13,
    "blue": 14,
  }

  var sum int = 0

  lines, _ := readInput("./input.txt")
  for _, line := range lines {
    id, isPossible := processGame(line, maximumCubes)
    if isPossible {
      sum += id
    }
  }
  return sum
}
